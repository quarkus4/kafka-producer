package com.sfeir.mbodin.kafkaproducer.event;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@QuarkusTest
class EventDataServiceTest {

    @Inject
    EventDataService eventDataService;

    @Test
    void fakeNext() {
        final EventData eventData = eventDataService.fakeNext();

        assertThat(eventData).isNotNull();
    }
}