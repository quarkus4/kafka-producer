package com.sfeir.mbodin.kafkaproducer.event;

import com.github.javafaker.Address;
import com.github.javafaker.Faker;
import com.github.javafaker.Options;
import com.github.javafaker.service.RandomService;
import com.google.protobuf.Message;
import com.sfeir.mbodin.kafkaproducer.faker.FakeValuesService;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@ApplicationScoped
public class EventDataService {

    private Faker faker;
    private UUID[] sources;
    private String[] nicknames;
    private DateFormat dateFormat;

    @Inject
    Validator validator;

    @Inject
    EventConfiguration eventConfiguration;

    @PostConstruct
    void initialize() {
        final RandomService randomService = new RandomService(null);
        faker = new Faker(new FakeValuesService(eventConfiguration.getLocale(), randomService),randomService);

        sources = new UUID[faker.random().nextInt(5, 10)];
        for (int i = sources.length - 1; i >= 0; i--) {
            sources[i] = UUID.randomUUID();
        }

        nicknames = new String[50];
        for (int i = nicknames.length - 1; i >= 0; i--) {
            nicknames[i] = faker.name().username();
        }

        dateFormat = new SimpleDateFormat("yyyy-MM-dd", eventConfiguration.getLocale());
    }

    EventData fakeNext() {
        final Options options = faker.options();

        final EventData eventData = EventData.builder()
                .source(options.option(sources))
                .operation(options.option(EventOperationKind.class))
                .card(CardData.builder()
                        .firstname(faker.name().firstName())
                        .lastname(faker.name().lastName())
                        .nickname(options.option(nicknames))
                        .birthday(dateFormat.format(faker.date().birthday(18, 67)))
                        .addresses(buildAddresses())
                        .emails(buildEmails())
                        .phones(buildPhones())
                        .build())
                .build();

        final Set<ConstraintViolation<EventData>> violations = validator.validate(eventData);

        if (violations.isEmpty()) {
            return eventData;
        }

        throw new ConstraintViolationException(violations);
    }

    public Message getNext() {
        return fakeNext().toMessage();
    }

    private List<AddressData> buildAddresses() {
        final List<AddressData> addresses = new ArrayList<>();
        final Address address = faker.address();
        final Options options = faker.options();

        for (int i = faker.random().nextInt(4) - 1; i >= 0; i--) {
            addresses.add(AddressData.builder()
                    .isDefault(faker.bool().bool())
                    .label(options.option("home", "work", "billing", "postal", "other"))
                    .street(address.streetAddress())
                    .locality(address.cityName())
                    .region(address.state())
                    .postcode(address.zipCode())
                    .country(address.country())
                    .build());
        }

        return addresses;
    }

    private List<EmailData> buildEmails() {
        final List<EmailData> emails = new ArrayList<>();
        final Options options = faker.options();

        for (int i = faker.random().nextInt(4) - 1; i >= 0; i--) {
            emails.add(EmailData.builder()
                    .isDefault(faker.bool().bool())
                    .label(options.option("personal", "work", "other"))
                    .value(faker.internet().safeEmailAddress())
                    .build());
        }

        return emails;
    }

    private List<PhoneData> buildPhones() {
        final List<PhoneData> phones = new ArrayList<>();
        final Options options = faker.options();

        for (int i = faker.random().nextInt(4) - 1; i >= 0; i--) {
            phones.add(PhoneData.builder()
                    .isDefault(faker.bool().bool())
                    .label(options.option("home", "work", "mobile", "fax", "pager", "other"))
                    .value(faker.phoneNumber().phoneNumber())
                    .build());
        }

        return phones;
    }
}
