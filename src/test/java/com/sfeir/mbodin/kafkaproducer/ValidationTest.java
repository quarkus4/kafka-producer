package com.sfeir.mbodin.kafkaproducer;

import com.sfeir.mbodin.kafkaproducer.event.*;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import javax.validation.Validator;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@QuarkusTest
class ValidationTest {

    @Inject
    Validator validator;

    @Test
    void email_validation_should_operate() {
        assertThat(validator.validate(EmailData.builder().build())).isNotEmpty().hasSize(1);
        assertThat(validator.validate(EmailData.builder().value("").build())).isNotEmpty().hasSize(1);
        assertThat(validator.validate(EmailData.builder().value("work").build())).isNotEmpty().hasSize(1);
        assertThat(validator.validate(EmailData.builder().value("doe.j@sfeir.com").build())).isEmpty();
    }

    @Test
    void phone_validation_should_operate() {
        assertThat(validator.validate(PhoneData.builder().build())).isNotEmpty().hasSize(2);
        assertThat(validator.validate(PhoneData.builder().value("").build())).isNotEmpty().hasSize(2);
        assertThat(validator.validate(PhoneData.builder().value("work").build())).isNotEmpty().hasSize(1);
        assertThat(validator.validate(PhoneData.builder().value("+33123456789").build())).isEmpty();
    }

    @Test
    void address_validation_should_operate() {
        assertThat(validator.validate(AddressData.builder().build())).isNotEmpty().hasSize(3);
        assertThat(validator.validate(AddressData.builder().street("").postcode("").locality("").build())).isNotEmpty().hasSize(3);
        assertThat(validator.validate(AddressData.builder().street("1 avenue de l'Europe").postcode("67300").locality("Schiltigheim").build())).isEmpty();
    }

    @Test
    void card_validation_should_operate() {
        assertThat(validator.validate(CardData.builder().build())).isNotEmpty().hasSize(1);
        assertThat(validator.validate(CardData.builder().nickname("jdoe").build())).isEmpty();
    }

    @Test
    void event_validation_should_operate() {
        final EventData.EventDataBuilder builder = EventData.builder();

        assertThat(validator.validate(builder.build())).isNotEmpty().hasSize(3);
        assertThat(validator.validate(builder.operation(EventOperationKind.CREATE).build())).isNotEmpty().hasSize(2);
        assertThat(validator.validate(builder.operation(EventOperationKind.CREATE).source(UUID.randomUUID()).build())).isNotEmpty().hasSize(1);
        assertThat(validator.validate(builder.operation(EventOperationKind.CREATE).source(UUID.randomUUID()).card(CardData.builder().nickname("jdoe").build()).build())).isEmpty();
    }
}
