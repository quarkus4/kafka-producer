package com.sfeir.mbodin.kafkaproducer;

import com.google.protobuf.Message;
import com.sfeir.mbodin.kafkaproducer.event.EventDataService;
import com.sfeir.mbodin.kafkaproducer.protobuff.EventData;
import com.sfeir.mbodin.kafkaproducer.topic.Initializer;
import io.quarkus.logging.Log;
import io.quarkus.runtime.Quarkus;
import io.quarkus.runtime.QuarkusApplication;
import io.quarkus.runtime.annotations.QuarkusMain;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.quartz.*;

import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Singleton;
import java.text.MessageFormat;
import java.util.Properties;

@QuarkusMain
public class Main {

    public static void main(String[] args) {
        Quarkus.run(KafkaProducerApplication.class, args);
    }

    public static class KafkaProducerApplication implements QuarkusApplication {
        @Inject
        Initializer topicInitializer;

        @Inject
        Scheduler quartz;

        private final Trigger trigger;

        @Produces
        @Singleton
        Properties producerProperties(final ApplicationConfiguration configuration) {
            final Properties properties = new Properties();

            properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, configuration.getBootstrapServers());
            properties.put(ProducerConfig.CLIENT_ID_CONFIG, configuration.getClientId());
            properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
            properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, CustomSerializer.class);

            return properties;
        }

        public KafkaProducerApplication(final ApplicationConfiguration configuration) {
            trigger = TriggerBuilder.newTrigger()
                    .withIdentity("kafka-event-producer")
                    .startNow()
                    .withSchedule(SimpleScheduleBuilder.simpleSchedule()
                            .withIntervalInMilliseconds(configuration.getHeartbeatInMs())
                            .repeatForever()
                    ).build();
        }

        @Override
        public int run(String... args) throws SchedulerException {
            if (topicInitializer.isInitialized()) {
                Log.trace("Start producing message");
                quartz.scheduleJob(JobBuilder.newJob(SendJob.class).build(), trigger);
            } else {
                Log.error("Topic is missing");
            }
            Quarkus.waitForExit();
            return 0;
        }

        public static class SendJob implements Job {
            @Inject
            EventDataService eventDataService;

            @Inject
            Initializer topicInitializer;

            @Inject
            Properties producerProperties;

            @Override
            public void execute(JobExecutionContext context) {
                final Message message = eventDataService.getNext();
                final Object field = message.getField(EventData.getDescriptor().findFieldByNumber(EventData.SOURCE_FIELD_NUMBER));
                final String key = MessageFormat.format("{0}_raw_operation", field);

                final ProducerRecord<String, Message> record = new ProducerRecord<>(topicInitializer.getName(), key, message);

                try(KafkaProducer<String, Message> kafkaProducer = new KafkaProducer<>(producerProperties)) {
                    kafkaProducer.send(record, (metadata, exception) -> {
                        if (exception != null) {
                            Log.error("", exception);
                        } else if (Log.isTraceEnabled()) {
                            Log.tracev("The message has been sent:\n{}", record.value().toString());
                        }
                    });
                }
            }
        }
    }
}
