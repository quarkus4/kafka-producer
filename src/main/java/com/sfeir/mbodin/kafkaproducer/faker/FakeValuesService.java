package com.sfeir.mbodin.kafkaproducer.faker;

import com.github.javafaker.service.FakeValuesInterface;
import com.github.javafaker.service.RandomService;
import com.github.javafaker.service.files.EnFile;

import java.util.*;

@SuppressWarnings("rawtypes")
public class FakeValuesService extends com.github.javafaker.service.FakeValuesService {
    private final List<FakeValuesInterface> fakeValuesList;

    public FakeValuesService(Locale locale, RandomService randomService) {
        super(locale, randomService);

        locale = normalizeLocale(locale);

        final List<Locale> locales = localeChain(locale);
        final List<FakeValuesInterface> all = new ArrayList<>(locales.size());

        for (final Locale l : locales) {
            boolean isEnglish = l.equals(Locale.ENGLISH);
            if (isEnglish) {
                FakeValuesGrouping fakeValuesGrouping = new FakeValuesGrouping();
                for (EnFile file : EnFile.getFiles()) {
                    fakeValuesGrouping.add(new FakeValues(l, file.getFile(), file.getPath()));
                }
                all.add(fakeValuesGrouping);
            } else {
                all.add(new FakeValues(locale));
            }
        }

        this.fakeValuesList = Collections.unmodifiableList(all);
    }

    @Override
    public Object fetchObject(String key) {
        String[] path = key.split("\\.");

        Object result = null;
        for (FakeValuesInterface fakeValuesInterface : fakeValuesList) {
            Object currentValue = fakeValuesInterface;
            for (int p = 0; currentValue != null && p < path.length; p++) {
                String currentPath = path[p];
                if (currentValue instanceof Map) {
                    currentValue = ((Map) currentValue).get(currentPath);
                } else  {
                    currentValue = ((FakeValuesInterface) currentValue).get(currentPath);
                }
            }
            result = currentValue;
            if (result != null) {
                break;
            }
        }
        return result;
    }

    private Locale normalizeLocale(Locale locale) {
        final String[] parts = locale.toString().split("[-_]");

        if (parts.length == 1) {
            return new Locale(parts[0]);
        } else {
            return new Locale(parts[0], parts[1]);
        }
    }
}
