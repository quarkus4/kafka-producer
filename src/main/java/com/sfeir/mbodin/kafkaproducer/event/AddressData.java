package com.sfeir.mbodin.kafkaproducer.event;

import com.sfeir.mbodin.kafkaproducer.SerializationUtil;
import lombok.Getter;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotBlank;
import java.util.function.Function;

@SuperBuilder
@Getter
public class AddressData extends AbstractContactInformation {
    private static final SerializationUtil.FieldMapper REGION_DESCRIPTOR= new SerializationUtil.FieldMapper(
            com.sfeir.mbodin.kafkaproducer.protobuff.AddressData.getDescriptor().findFieldByNumber(com.sfeir.mbodin.kafkaproducer.protobuff.AddressData.REGION_FIELD_NUMBER)
            , Function.identity()
    );

    private static final SerializationUtil.FieldMapper COUNTRY_DESCRIPTOR= new SerializationUtil.FieldMapper(
            com.sfeir.mbodin.kafkaproducer.protobuff.AddressData.getDescriptor().findFieldByNumber(com.sfeir.mbodin.kafkaproducer.protobuff.AddressData.COUNTRY_FIELD_NUMBER)
            , Function.identity()
    );

    private static final SerializationUtil.FieldMapper LABEL_DESCRIPTOR = new SerializationUtil.FieldMapper(
            com.sfeir.mbodin.kafkaproducer.protobuff.AddressData.getDescriptor().findFieldByNumber(com.sfeir.mbodin.kafkaproducer.protobuff.AddressData.LABEL_FIELD_NUMBER)
            , Function.identity()
    );

    @NotBlank
    private String street;
    @NotBlank
    private String locality;
    @NotBlank
    private String postcode;
    private String region;
    private String country;

    protected com.sfeir.mbodin.kafkaproducer.protobuff.AddressData toMessage() {
        final com.sfeir.mbodin.kafkaproducer.protobuff.AddressData.Builder builder = com.sfeir.mbodin.kafkaproducer.protobuff.AddressData.newBuilder();

        SerializationUtil.addIfDefined(builder,REGION_DESCRIPTOR, region);
        SerializationUtil.addIfDefined(builder,COUNTRY_DESCRIPTOR, country);
        SerializationUtil.addIfDefined(builder, LABEL_DESCRIPTOR, getLabel());

        return builder
                .setIsDefault(isDefault())
                .setStreet(street)
                .setLocality(locality)
                .setPostcode(postcode)
                .build();
    }
}
