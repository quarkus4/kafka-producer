package com.sfeir.mbodin.kafkaproducer.event;

import com.google.protobuf.Descriptors;
import com.google.protobuf.Message;
import lombok.Builder;
import lombok.Getter;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Builder
@Getter
public class EventData {
    private static final Descriptors.FieldDescriptor CARD_DESCRIPTOR = com.sfeir.mbodin.kafkaproducer.protobuff.EventData.getDescriptor()
            .findFieldByNumber(com.sfeir.mbodin.kafkaproducer.protobuff.EventData.CARD_FIELD_NUMBER);

    @NotNull
    private EventOperationKind operation;
    @NotNull
    private UUID source;
    @NotNull
    private CardData card;

    public Message toMessage() {
        return com.sfeir.mbodin.kafkaproducer.protobuff.EventData.newBuilder()
                .setOperation(operation.toEnumValue())
                .setSource(source.toString())
                .setField(CARD_DESCRIPTOR, card.toMessage())
                .build();
    }
}
