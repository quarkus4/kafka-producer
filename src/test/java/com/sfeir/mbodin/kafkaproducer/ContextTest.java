package com.sfeir.mbodin.kafkaproducer;

import com.sfeir.mbodin.kafkaproducer.event.EventDataService;
import com.sfeir.mbodin.kafkaproducer.topic.Initializer;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;

import static org.assertj.core.api.Assertions.assertThat;

@QuarkusTest
public class ContextTest {

    @Inject
    EventDataService eventDataService;

    @Inject
    Initializer topicInitializer;

    @Test
    void contextLoads() {
        assertThat(eventDataService).isNotNull();
        assertThat(topicInitializer).isNotNull();
    }
}
