package com.sfeir.mbodin.kafkaproducer.event;

import org.junit.jupiter.api.Test;

import java.util.UUID;
import java.util.function.Consumer;

import static com.sfeir.mbodin.kafkaproducer.protobuff.EventOperationKind.CREATE;
import static org.assertj.core.api.Assertions.assertThat;


class EventDataTest {

    @Test
    void toMessage() {
        final CardData card = CardData.builder()
                .nickname("jdoe")
                .build();
        final EventData event = EventData.builder()
                .operation(EventOperationKind.CREATE)
                .source(UUID.randomUUID())
                .card(card)
                .build();

        final Consumer<com.sfeir.mbodin.kafkaproducer.protobuff.EventData> requirements1 = ser -> {
            assertThat(ser.getOperation()).isEqualByComparingTo(CREATE);
            assertThat(ser.hasSource()).isTrue();
            assertThat(ser.getCard()).isNotNull()
                    .satisfies(c -> {
                        assertThat(c.getNickname()).isEqualTo("jdoe");
                        assertThat(c.hasFirstname()).isFalse();
                        assertThat(c.hasLastname()).isFalse();
                        assertThat(c.hasBirthday()).isFalse();
                        assertThat(c.getEmailsCount()).isZero();
                        assertThat(c.getPhonesCount()).isZero();
                        assertThat(c.getAddressesCount()).isZero();
                    })
            ;
        };

        assertThat(event.toMessage()).isNotNull()
                .isInstanceOfSatisfying(com.sfeir.mbodin.kafkaproducer.protobuff.EventData.class, requirements1);

        // Add a birthday, a phone and an address
        event.getCard().setBirthday("1970-01-01");

        event.getCard().getPhones().add(PhoneData.builder()
                .value("+33123456789")
                .label("office")
                .isDefault(true)
                .build());

        event.getCard().getAddresses().add(AddressData.builder()
                        .street("1 avenue de l'Europe")
                        .postcode("67300")
                        .locality("Schiltigheim")
                        .isDefault(false)
                .build());

        final Consumer<com.sfeir.mbodin.kafkaproducer.protobuff.EventData> requirements2 = ser -> {
            assertThat(ser.getOperation()).isEqualByComparingTo(CREATE);
            assertThat(ser.hasSource()).isTrue();
            assertThat(ser.getCard()).isNotNull()
                    .satisfies(c -> {
                        assertThat(c.getNickname()).isEqualTo("jdoe");
                        assertThat(c.hasFirstname()).isFalse();
                        assertThat(c.hasLastname()).isFalse();
                        assertThat(c.getBirthday()).isEqualTo("1970-01-01");
                        assertThat(c.getEmailsCount()).isZero();
                        assertThat(c.getPhonesCount()).isEqualTo(1);
                        assertThat(c.getAddressesCount()).isEqualTo(1);

                        assertThat(c.getPhones(0)).satisfies(p -> {
                            assertThat(p.getIsDefault()).isTrue();
                            assertThat(p.getValue()).isEqualTo("+33123456789");
                            assertThat(p.getLabel()).isEqualTo("office");
                        });
                    })
            ;
        };

        assertThat(event.toMessage()).isNotNull()
                .isInstanceOfSatisfying(com.sfeir.mbodin.kafkaproducer.protobuff.EventData.class, requirements2);
    }
}