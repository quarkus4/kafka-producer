package com.sfeir.mbodin.kafkaproducer;

import io.smallrye.config.ConfigMapping;
import io.smallrye.config.WithName;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;

@ConfigMapping(prefix = "application")
public interface ApplicationConfiguration {
    @NotBlank()
    @WithName("bootstrap-servers")
    String getBootstrapServers();

    @NotBlank
    @WithName("client-id")
    String getClientId();

    @Positive
    @WithName("heartbeat-in-ms")
    long getHeartbeatInMs();
}
