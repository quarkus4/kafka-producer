package com.sfeir.mbodin.kafkaproducer;

import com.google.protobuf.Descriptors;
import com.google.protobuf.GeneratedMessageV3;
import lombok.Getter;

import java.util.Collection;
import java.util.function.Function;

public class SerializationUtil {

    @SuppressWarnings({"rawtypes", "unchecked"})
    public static void addIfDefined(GeneratedMessageV3.Builder builder, FieldMapper fieldMapper, Object value) {
        if (value instanceof Collection) {
            ((Collection) value).stream().map(fieldMapper.getMapper())
                    .forEachOrdered(message -> builder.addRepeatedField(fieldMapper.getDescriptor(), message));
        }
        else if (value instanceof String) {
            String string = (String) value;
            if (!string.isBlank()) {
                builder.setField(fieldMapper.getDescriptor(), fieldMapper.mapper.apply(string));
            }
        }
    }

    public static class FieldMapper {

        @Getter
        private final Function<Object, Object> mapper;
        @Getter
        private final Descriptors.FieldDescriptor descriptor;

        public FieldMapper(Descriptors.FieldDescriptor descriptor, Function<Object, Object> mapper) {
            this.mapper = mapper;
            this.descriptor = descriptor;
        }
    }
}
