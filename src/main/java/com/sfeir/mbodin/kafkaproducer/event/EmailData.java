package com.sfeir.mbodin.kafkaproducer.event;

import com.sfeir.mbodin.kafkaproducer.SerializationUtil;
import com.sfeir.mbodin.kafkaproducer.protobuff.ContactInformation;
import lombok.Getter;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@SuperBuilder
@Getter
public class EmailData extends AbstractContactInformation {
    @NotBlank
    @Email
    private String value;

    public ContactInformation toMessage() {
        final ContactInformation.Builder builder = ContactInformation.newBuilder();

        SerializationUtil.addIfDefined(builder, AbstractContactInformation.LABEL_DESCRIPTOR, getLabel());

        return builder
                .setIsDefault(isDefault())
                .setValue(value)
                .build();
    }
}
