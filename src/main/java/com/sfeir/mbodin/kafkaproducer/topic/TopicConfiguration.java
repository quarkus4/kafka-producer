package com.sfeir.mbodin.kafkaproducer.topic;

import io.smallrye.config.ConfigMapping;
import io.smallrye.config.WithName;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;

@ConfigMapping(prefix = "topic")
public interface TopicConfiguration {

    @WithName("name")
    @NotBlank
    String getName();

    @WithName("num-partition")
    @Positive
    Integer getNumPartition();

    @WithName("replication-factor")
    Short getReplicationFactor();

    @Positive
    @WithName("retention-in-bytes")
    Long getRetentionInByte();
}
