package com.sfeir.mbodin.kafkaproducer.event;

import io.smallrye.config.ConfigMapping;
import io.smallrye.config.WithName;

import java.util.Locale;

@ConfigMapping(prefix = "event")
public interface EventConfiguration {

    @WithName("locale")
    Locale getLocale();
}
