package com.sfeir.mbodin.kafkaproducer.event;

import com.sfeir.mbodin.kafkaproducer.SerializationUtil;
import com.sfeir.mbodin.kafkaproducer.protobuff.ContactInformation;
import lombok.Getter;
import lombok.experimental.SuperBuilder;

import java.util.function.Function;

@SuperBuilder
@Getter
public abstract class AbstractContactInformation {
    protected static final SerializationUtil.FieldMapper LABEL_DESCRIPTOR= new SerializationUtil.FieldMapper(
            com.sfeir.mbodin.kafkaproducer.protobuff.ContactInformation.getDescriptor().findFieldByNumber(ContactInformation.LABEL_FIELD_NUMBER)
            , Function.identity()
    );

    private String label;
    private boolean isDefault;
}
