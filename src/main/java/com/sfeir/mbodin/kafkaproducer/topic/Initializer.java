package com.sfeir.mbodin.kafkaproducer.topic;

import com.sfeir.mbodin.kafkaproducer.ApplicationConfiguration;
import io.quarkus.logging.Log;
import org.apache.kafka.clients.admin.Admin;
import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.admin.NewTopic;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutionException;

@ApplicationScoped
public class Initializer {
    @Inject
    TopicConfiguration topicConfiguration;
    @Inject
    ApplicationConfiguration applicationConfiguration;

    public boolean isInitialized() {
        boolean exists = false;

        try (final Admin admin = Admin.create(asProperties())) {
            if (Log.isTraceEnabled()) Log.tracev("Topic \"{}\" is being checked", topicConfiguration.getName());

            exists = admin.listTopics().names()
                    .thenApply(names -> names.contains(topicConfiguration.getName()))
                    .get();

            if (!exists) {
                if (Log.isTraceEnabled()) Log.tracev("Topic \"{}\" need to be created", topicConfiguration.getName());
                final NewTopic topic = new NewTopic(topicConfiguration.getName(), topicConfiguration.getNumPartition(), topicConfiguration.getReplicationFactor())
                        .configs(asCreatingMap());

                admin.createTopics(Collections.singleton(topic)).all().get();

                if (Log.isTraceEnabled()) Log.tracev("Topic \"{}\" has been created", topicConfiguration.getName());

                exists = true;
            } else if (Log.isTraceEnabled()) Log.tracev("Topic \"{}\" already exists", topicConfiguration.getName());

        } catch (RuntimeException | InterruptedException | ExecutionException exception) {
            Log.error("Initialization failed", exception);
        }

        return exists;
    }

    private Properties asProperties() {
        final Properties properties = new Properties();

        properties.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, applicationConfiguration.getBootstrapServers());
        return properties;
    }

    private Map<String, String> asCreatingMap() {
        final Map<String, String> configs = new HashMap<>();

        // Configure retention by size on disk
        configs.put("retention.bytes", topicConfiguration.getRetentionInByte().toString());
        configs.put("segment.bytes", String.valueOf(topicConfiguration.getRetentionInByte() / 5));

        return configs;
    }

    public String getName() {
        return topicConfiguration.getName();
    }
}
